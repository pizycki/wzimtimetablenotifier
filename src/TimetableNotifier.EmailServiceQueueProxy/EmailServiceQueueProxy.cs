﻿using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using TimetableNotifier.Email.Contracts;

namespace TimetableNotifier.Email.QueueBus
{
    public class EmailServiceQueueProxy : IEmailService
    {
        private readonly QueueConfiguration _queueConfig;

        public EmailServiceQueueProxy(QueueConfiguration queueConfig)
        {
            _queueConfig = queueConfig;
        }

        public void Setup()
        {
            QueueManager.CreateQueueOnAzure(_queueConfig);
        }

        public async void SendEmail(IEmailMessage emailMessage)
        {
            var serializedMessage = JsonConvert.SerializeObject(emailMessage);
            var message = new BrokeredMessage(serializedMessage);

            var queue = QueueManager.CreateQueueClient(_queueConfig);
            await queue.SendAsync(message);
        }


    }
}
