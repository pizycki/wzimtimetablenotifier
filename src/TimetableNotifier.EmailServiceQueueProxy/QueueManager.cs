﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;

namespace TimetableNotifier.Email.QueueBus
{
    public class QueueManager
    {
        public static void CreateQueueOnAzure(QueueConfiguration queueConfig)
        {
            var namespaceManager = NamespaceManager.CreateFromConnectionString(queueConfig.ConnectionString);

            if (!namespaceManager.QueueExists(queueConfig.QueueName))
            {
                namespaceManager.CreateQueue(queueConfig.QueueName);
            }
        }

        public static QueueClient CreateQueueClient(QueueConfiguration queueConfig)
        {
            return QueueClient.CreateFromConnectionString(queueConfig.ConnectionString, queueConfig.QueueName);
        }
    }
}