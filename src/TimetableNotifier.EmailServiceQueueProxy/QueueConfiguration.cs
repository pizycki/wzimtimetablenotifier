﻿namespace TimetableNotifier.Email.QueueBus
{
    public class QueueConfiguration
    {
        public QueueConfiguration(string connectionString, string queueName)
        {
            ConnectionString = connectionString;
            QueueName = queueName;
        }

        public string ConnectionString { get; private set; }
        public string QueueName { get; private set; }
    }
}