﻿using System;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using TimetableNotifier.Email.Common;
using TimetableNotifier.Email.Contracts;

namespace TimetableNotifier.Email.QueueBus
{
    public class EmailServiceQueueClient
    {
        private readonly QueueConfiguration _queueConfig;
        private readonly IEmailService _emailService;

        public EmailServiceQueueClient(QueueConfiguration queueConfig, IEmailService emailService)
        {
            _queueConfig = queueConfig;
            _emailService = emailService;
        }

        public void Setup()
        {
            QueueManager.CreateQueueOnAzure(_queueConfig);
            var client = QueueManager.CreateQueueClient(_queueConfig);

            // Configure the callback options.
            var options = new OnMessageOptions();
            options.AutoComplete = false;
            options.AutoRenewTimeout = TimeSpan.FromSeconds(1);

            // Callback to handle received messages.
            client.OnMessage(message =>
            {
                try
                {
                    HandleQueueMessage(message);

                    // Remove message from queue.
                    message.Complete();
                }
                finally
                {
                    // Unlock message in queue.
                    message.Abandon();
                }
            }, options);
        }

        private static IEmailMessage ParseEmailMessage(BrokeredMessage message)
        {
            var serializedEmailMessage = message.GetBody<string>();
            var emailMessage = JsonConvert.DeserializeObject<EmailMessage>(serializedEmailMessage);
            return emailMessage;
        }

        private void HandleQueueMessage(BrokeredMessage message)
        {
            var emailMessage = ParseEmailMessage(message);
            _emailService.SendEmail(emailMessage);
        }
    }
}
