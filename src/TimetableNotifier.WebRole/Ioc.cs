﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Abstractions;
using Autofac;
using Autofac.Core;
using Microsoft.Azure;
using TimetableNotifier.DAL;
using TimetableNotifier.DAL.SQL;
using TimetableNotifier.Email;
using TimetableNotifier.Email.Contracts;
using TimetableNotifier.Email.QueueBus;
using TimetableNotifier.Html;
using TimetableNotifier.Subscribers;
using TimetableNotifier.Wzim;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.WebRole
{
    public static class Ioc
    {
        private static IContainer _container;

        public static ContainerBuilder GetBuilder()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<HtmlService>()
                .AsImplementedInterfaces();

            //builder.RegisterInstance(LogManager.GetLogger("TimetableNotifier.Worker.Logger"))
            //        .AsImplementedInterfaces()
            //        .SingleInstance();

            builder.RegisterModule<InfrastuctureModule>();
            builder.RegisterModule<TimetablesModule>();
            builder.RegisterModule<SubscribersModule>();
            builder.RegisterModule<WzimTimetableCheckerModule>();

            //builder.RegisterModule<TestEmailServiceModule>();
            builder.RegisterModule<EmailServiceModule>();

            return builder;
        }

        public static IContainer Container
        {
            get
            {
                if (_container == null)
                {
                    _container = GetBuilder().Build();
                }
                return _container;
            }
        }
    }
    internal class TimetablesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TimetablesRepository>()
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
    internal class InfrastuctureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //#if DEBUG
            //            if (!CloudConfigurationManager.GetSetting("SqlDbConnString").ToUpper().Contains("EXPRESS"))
            //            {
            //                throw new Exception("While debbuging, conn string must contains localhost.");
            //            }
            //#endif
            builder.RegisterType<SqlDbProvider>()
                .AsImplementedInterfaces()
                .WithParameter("connString", CloudConfigurationManager.GetSetting("SqlDbConnString"));

        }
    }
    internal class SubscribersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SubscribersService>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterType<SubscribersRepository>()
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }

    internal class WzimTimetableCheckerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WzimTimetableChecker>()
                .As<IWzimTimetableChecker>()
                .SingleInstance();

            var queueConfig = EmailServiceQueueBusConfiguration.GetQueueConfig();
            builder.RegisterType<EmailServiceQueueProxy>()
                .WithParameter("queueConfig", queueConfig)
                .OnActivated(args =>
                {
                    args.Instance.Setup();
                    // Resolving results with invocing singleton instance of Queue client which will await for incoming msgs.
                    args.Context.Resolve<EmailServiceQueueClient>();
                });

            builder.RegisterType<EmailNotifier>()
                .WithParameter(new ResolvedParameter(
                    (pi, ctx) => pi.ParameterType == typeof(IEmailService),
                    (pi, ctx) => ctx.Resolve<EmailServiceQueueProxy>()))
                .AsImplementedInterfaces();

            builder.RegisterType<WzimTimetableNotifier>()
                .As<IWzimTimetableNotifier>()
                .SingleInstance();
        }
    }

    internal class EmailServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FileSystem>().As<IFileSystem>();

            var root = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);

            builder.RegisterType<MailTemplatesProvider>()
                .AsImplementedInterfaces()
                .WithParameter("root", root);

            RegisterEmailService(builder);

            builder.Register(ctx =>
                            {
                                var emailService = ctx.Resolve<EmailService>();
                                var emailQueueClnt = new EmailServiceQueueClient(EmailServiceQueueBusConfiguration.GetQueueConfig(), emailService);
                                emailQueueClnt.Setup();
                                return emailQueueClnt;
                            })
                    .AsSelf()
                    .SingleInstance();
        }

        protected virtual void RegisterEmailService(ContainerBuilder builder)
        {
            builder.RegisterType<EmailService>()
                .WithParameter("apiKey", CloudConfigurationManager.GetSetting("SendGridApiKey"))
                .AsSelf();
        }
    }

    internal static class EmailServiceQueueBusConfiguration
    {
        public static QueueConfiguration GetQueueConfig()
        {
            var queueName = CloudConfigurationManager.GetSetting("EmailQueueName");
            var connectionString = CloudConfigurationManager.GetSetting("ServiceBusNamespaceConnString");
            return new QueueConfiguration(connectionString, queueName);
        }
    }

    internal class TestEmailServiceModule : EmailServiceModule
    {
        protected override void RegisterEmailService(ContainerBuilder builder)
        {
            builder.RegisterType<TestEmailService>().AsSelf();
        }
    }
    internal class TestEmailService : IEmailService
    {
        public void SendEmail(IEmailMessage emailMessage)
        {
            Debug.WriteLine("Email sent! at " + DateTime.Now + ", " + emailMessage);
        }
    }
}