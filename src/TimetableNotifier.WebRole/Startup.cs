﻿using Microsoft.Owin;
using Owin;
using TimetableNotifier.WebRole.Hangfire;

[assembly: OwinStartup(typeof(TimetableNotifier.WebRole.Startup))]

namespace TimetableNotifier.WebRole
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.ConfigureHangfire();
        }
    }
}