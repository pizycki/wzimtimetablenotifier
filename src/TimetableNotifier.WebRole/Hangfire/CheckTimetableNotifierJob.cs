﻿using System.Threading.Tasks;
using Autofac;
using TimetableNotifier.Common;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.WebRole.Hangfire
{
    internal class CheckTimetableNotifierJob
    {
        public static void Perform()
        {
            var notifier = Ioc.Container.Resolve<IWzimTimetableNotifier>();
            var checker = Ioc.Container.Resolve<IWzimTimetableChecker>();
            var repository = Ioc.Container.Resolve<ITimetableRepository>();

            string timetablesListUrl = AppConfiguration.WzimTimetableUrl;
            var newTimetables = checker.GetNewTimetables(timetablesListUrl).Result;
            
            foreach (var @new in newTimetables)
            {
                // Add new timetable to repository
                repository.AddTimetable(@new);

                // Notify subscribers about new timetable
                notifier.NotifyAboutNewTimetable(@new);
            }
        }
    }
}