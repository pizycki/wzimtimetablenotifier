﻿using Hangfire;
using Hangfire.Dashboard;
using Hangfire.SqlServer;
using Microsoft.Azure;
using Owin;

namespace TimetableNotifier.WebRole.Hangfire
{
    internal static class HangfireConfiguration
    {
        public static void ConfigureHangfire(this IAppBuilder app)
        {
            ConfigureStorage();
            app.UseHangfireServer();
            app.ConfigureDashboard();
            ConfigureJobs();
        }

        private static void ConfigureDashboard(this IAppBuilder app, string dashbouardPath = "/hangfire")
        {
            var authFilter = ConfigureAuthorizationFilter();
            var options = new DashboardOptions
            {
                AuthorizationFilters = new IAuthorizationFilter[] { authFilter },
            };
            app.UseHangfireDashboard(dashbouardPath, options);
        }

        private static BasicAuthAuthorizationFilter ConfigureAuthorizationFilter()
        {
            return new BasicAuthAuthorizationFilter(
                                new BasicAuthAuthorizationFilterOptions
                                {
                                    // Secure connection for dashboard
                                    RequireSsl = false,
                                    SslRedirect = false,
                                    
                                    // Case sensitive login checking
                                    LoginCaseSensitive = false,
                                    
                                    // Users
                                    Users = new[]
                                    {
                                        new BasicAuthAuthorizationUser
                                        {
                                            Login = "admin",
                                            // Password as SHA1 hash
                                            Password = new byte[]{ 0x69,0x00,0x83,0x92,0xe6,0x3b,0xc2,0x96,0xb2,0xe7,0x67,0x90,0x87,0x14,0x2c,0x8f,0x1b,0x3a,0xa0,0x62 }
                                        }
                                    }
                                });

        }

        private static void ConfigureJobs()
        {
            HangfireJobs.RegisterCheckTimetableNotifierJob();
        }
        
        private static void ConfigureStorage()
        {
            var sqlDbConnString = CloudConfigurationManager.GetSetting("SqlDbConnString");
            var sqlStorage = new SqlServerStorage(sqlDbConnString);
            GlobalConfiguration.Configuration.UseStorage(sqlStorage);
        }
    }

}