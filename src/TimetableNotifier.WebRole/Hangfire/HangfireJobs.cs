﻿using Hangfire;
using TimetableNotifier.Common;

namespace TimetableNotifier.WebRole.Hangfire
{
    internal class HangfireJobs
    {
        public static void RegisterCheckTimetableNotifierJob()
        {
            RecurringJob.AddOrUpdate(() => CheckTimetableNotifierJob.Perform(), AppConfiguration.CheckTimetableNotifierJobInterval);
        }
    }
}