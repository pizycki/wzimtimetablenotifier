using System;

namespace TimetableNotifier.Wzim
{
    public interface IDateTimeProvider
    {
        DateTime GetCurrentDateTime();
    }
}