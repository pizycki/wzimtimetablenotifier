﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TimetableNotifier.Common;

namespace TimetableNotifier.Wzim.Contracts
{
    public enum TypeOfStudies
    {
        [Display(Name = "TypeOfStudies_FullTime", ResourceType = typeof(Resources))]
        FullTime,
        [Display(Name = "TypeOfStudies_PartTime", ResourceType = typeof(Resources))]
        PartTime
    }

    public static class TypeOfStudiesExt
    {
        public static readonly IReadOnlyDictionary<TypeOfStudies, string> TypeOfStudiesDict = new Dictionary<TypeOfStudies, string>
        {
            {TypeOfStudies.FullTime, "STAC"},
            {TypeOfStudies.PartTime, "ZAO"}
        };

        public static string GetAbbrv(this TypeOfStudies @type)
        {
            return TypeOfStudiesDict[@type];
        }

        public static TypeOfStudies Parse(string name)
        {
            TypeOfStudies type;
            if (Enum.TryParse(name, out type))
                return type;

            throw new ArgumentException("Unknown name.");
        }

        public static TypeOfStudies ParseByAbbrv(string abbrv)
        {
            foreach (var key in from key in TypeOfStudiesDict.Keys
                                let val = TypeOfStudiesDict[key]
                                where val == abbrv
                                select key)
            {
                return key;
            }

            throw new ArgumentException("Unknown abbrv.");
        }

        public static IEnumerable<TypeOfStudies> GetTypeOfStudiesCollection(this TypeOfStudies @types)
        {
            var str = types.ToString().Replace(" ", "");

            if (string.IsNullOrEmpty(str))
                throw new Exception("Nothing to parse.");

            var arr = str.Split(',');

            return arr.Select(x => (TypeOfStudies)Enum.Parse(typeof(TypeOfStudies), x));
        }
    }
}