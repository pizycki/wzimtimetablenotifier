using System;

namespace TimetableNotifier.Wzim.Contracts
{
    public interface ITimetable
    {
        TypeOfStudies TypeOfStudies { get; }
        string Href { get; }
        string Label { get; }
        DateTime PublishDateUtc { get; }
    }
}