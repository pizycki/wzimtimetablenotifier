namespace TimetableNotifier.Wzim.Contracts
{
    public interface IWzimTimetableNotifier
    {
        void NotifyAboutNewTimetable(ITimetable timetable);
    }
}