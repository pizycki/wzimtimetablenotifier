using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Wzim
{
    public interface ITimetabelLabelGenerator
    {
        string GenerateLabel(TypeOfStudies type);
    }
}