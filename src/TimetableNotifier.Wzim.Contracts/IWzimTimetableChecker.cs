﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TimetableNotifier.Wzim.Contracts
{
    public interface IWzimTimetableChecker
    {
        Task<IEnumerable<ITimetable>> GetNewTimetables(string timetablesListUrl);
    }
}