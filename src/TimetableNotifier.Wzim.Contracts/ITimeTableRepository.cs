﻿using System.Threading.Tasks;

namespace TimetableNotifier.Wzim.Contracts
{
    public interface ITimetableRepository
    {
        Task<ITimetable> GetLatestTimetableFor(TypeOfStudies typeOfStudies);
        Task<bool> AddTimetable(ITimetable timetable);
    }
}