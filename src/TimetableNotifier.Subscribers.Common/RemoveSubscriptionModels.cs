﻿namespace TimetableNotifier.Subscribers.Common
{
    public class RemoveSubscriptionsByEmailModel
    {
        public string Email { get; set; }
    }

    public class RemoveSubscriptionsByEmailResult
    {
        public bool Success { get; set; }
        public RemoveSubscriptionsByEmailModel Model { get; set; }
    }
}
