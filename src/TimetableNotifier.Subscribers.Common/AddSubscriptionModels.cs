﻿using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Subscribers.Common
{
    public class AddSubscriptionModel
    {
        public string Email { get; set; }
        public TypeOfStudies TypeOfStudies { get; set; }
    }

    public class AddSubscriptionResult
    {
        public bool SubscribtionAlreadyExists { get; set; }
        public bool SubscribtionAdded { get; set; }

        public AddSubscriptionModel Model { get; set; }
    }
}
