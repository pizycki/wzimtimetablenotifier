﻿using System.Threading.Tasks;
using TimetableNotifier.Email.Contracts;

namespace TimetableNotifier.Subscribers.Contracts
{
    public interface ISubscribersRepository : INotificationSubscribersProvider
    {
        Task<bool> AddSubscribtionAsync(string email, string typeOfStudies);
        Task<bool> RemoveSubscribtionsByEmailAsync(string email);
        Task<bool> SubscribtionExistsAsync(string email, string typeOfStudies);
    }
}