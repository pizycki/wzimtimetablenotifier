﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Subscribers.Contracts
{
    public interface INotificationSubscribersProvider
    {
        Task<IEnumerable<string>> GetSubscribersEmails(TypeOfStudies typeOfStudies);
    }
}