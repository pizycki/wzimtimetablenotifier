using TimetableNotifier.Subscribers.Common;

namespace TimetableNotifier.Subscribers.Contracts
{
    public interface ISubscribersService
    {
        AddSubscriptionResult AddSubscribtion(AddSubscriptionModel model);
        RemoveSubscriptionsByEmailResult RemoveSubscribtionsByEmail(RemoveSubscriptionsByEmailModel model);
    }
}