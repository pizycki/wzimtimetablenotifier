﻿using NSubstitute;
using NUnit.Framework;
using TimetableNotifier.Subscribers.Common;
using TimetableNotifier.Subscribers.Contracts;

namespace TimetableNotifier.Subscribers.Tests
{
    [TestFixture]
    public class RemoveSubscriptionsByEmailService_Tests
    {
        const string email = "test@email.com";

        private RemoveSubscriptionsByEmailService_Tests_Fixture _fixture;

        [SetUp]
        public void Setup()
        {
            _fixture = new RemoveSubscriptionsByEmailService_Tests_Fixture();
        }

        [TearDown]
        public void TearDown()
        {
            _fixture = null;
        }

        [Test]
        public void Removes_subscriber_from_repository_and_all_his_subscriptions()
        {
            _fixture.Repistory_will_remove_all_emails(email);

            var service = _fixture.CreateRemoveSubscriptionsByEmailService();
            var model = _fixture.CreateRemoveSubscriptionsByEmailModel(email);
            var res = service.RemoveSubscribtionsByEmailAsync(model).Result;

            Assert.IsTrue(res.Success);
            _fixture.SubscribersRepository.Received().RemoveSubscribtionsByEmailAsync(email);
        }


        [Test]
        public void Does_not_removes_subscribtions_from_repository_due_database_error()
        {
            _fixture.Repistory_will_crash(email);

            var service = _fixture.CreateRemoveSubscriptionsByEmailService();
            var model = _fixture.CreateRemoveSubscriptionsByEmailModel(email);
            var res = service.RemoveSubscribtionsByEmailAsync(model).Result;

            Assert.IsFalse(res.Success);
            _fixture.SubscribersRepository.Received().RemoveSubscribtionsByEmailAsync(email);
        }

    }

    internal class RemoveSubscriptionsByEmailService_Tests_Fixture
    {
        internal ISubscribersRepository SubscribersRepository { get; private set; }

        public RemoveSubscriptionsByEmailService_Tests_Fixture()
        {
            SubscribersRepository = Substitute.For<ISubscribersRepository>();
        }

        public RemoveSubscriptionsByEmailService CreateRemoveSubscriptionsByEmailService()
        {
            return new RemoveSubscriptionsByEmailService(SubscribersRepository);
        }


        public void Repistory_will_remove_all_emails(string email)
        {
            SubscribersRepository.RemoveSubscribtionsByEmailAsync(email).Returns(true);
        }

        public RemoveSubscriptionsByEmailModel CreateRemoveSubscriptionsByEmailModel(string email)
        {
            return new RemoveSubscriptionsByEmailModel
            {
                Email = email
            };
        }

        public void Repistory_will_crash(string email)
        {
            SubscribersRepository.RemoveSubscribtionsByEmailAsync(email).Returns(false);
        }
    }
}
