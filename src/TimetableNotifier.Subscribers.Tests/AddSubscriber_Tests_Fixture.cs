﻿using NSubstitute;
using TimetableNotifier.Subscribers.Common;
using TimetableNotifier.Subscribers.Contracts;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Subscribers.Tests
{
    internal class AddSubscriber_Tests_Fixture
    {
        internal ISubscribersRepository SubscribersRepository { get; private set; }

        public AddSubscriber_Tests_Fixture()
        {
            SubscribersRepository = Substitute.For<ISubscribersRepository>();
        }

        internal AddSubscribtionService CreateAddSubscribtionService()
        {
            return new AddSubscribtionService(SubscribersRepository);
        }

        internal void SubscriberAlreadyExistsInRepository(string email, TypeOfStudies typeOfStudies)
        {
            SubscribersRepository.SubscribtionExistsAsync(email, typeOfStudies.GetAbbrv()).Returns(true);
        } 
        internal void SubscriberDoesntExistInRepository(string email, TypeOfStudies typeOfStudies)
        {
            SubscribersRepository.SubscribtionExistsAsync(email, typeOfStudies.GetAbbrv()).Returns(false);
        }

        internal void RepositoryWillAddSubscriber(string email, TypeOfStudies typeOfStudies)
        {
            SubscribersRepository.AddSubscribtionAsync(email, typeOfStudies.GetAbbrv()).Returns(true);
        }

        internal AddSubscriptionModel CreateAddSubscriptionModel(string email, TypeOfStudies typeOfStudies)
        {
            return new AddSubscriptionModel
            {
                Email = email,
                TypeOfStudies = typeOfStudies
            };
        }
    }
}