﻿using NSubstitute;
using NUnit.Framework;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Subscribers.Tests
{
    [TestFixture]
    public class AddSubscriber_Tests
    {
        const string email = "test@email.com";
        const TypeOfStudies fullTimeTypeOfStudies = TypeOfStudies.FullTime;

        private AddSubscriber_Tests_Fixture _fixture;

        [SetUp]
        public void Setup()
        {
            _fixture = new AddSubscriber_Tests_Fixture();
        }

        [TearDown]
        public void TearDown()
        {
            _fixture = null;
        }

        [Test]
        public void Adds_subscriber_to_repository()
        {
            _fixture.SubscriberDoesntExistInRepository(email, fullTimeTypeOfStudies);
            _fixture.RepositoryWillAddSubscriber(email, fullTimeTypeOfStudies);

            var service = _fixture.CreateAddSubscribtionService();
            var model = _fixture.CreateAddSubscriptionModel(email, fullTimeTypeOfStudies);
            var res = service.AddSubscribtionAsync(model).Result;
            
            Assert.IsTrue(res.SubscribtionAdded);
            Assert.IsFalse(res.SubscribtionAlreadyExists);
            _fixture.SubscribersRepository.Received().AddSubscribtionAsync(model.Email, model.TypeOfStudies.GetAbbrv());
        }

        [Test]
        public void Doesnt_adds_subscriber_to_repository_when_its_already_present()
        {
            _fixture.SubscriberAlreadyExistsInRepository(email, fullTimeTypeOfStudies);

            var service = _fixture.CreateAddSubscribtionService();
            var model = _fixture.CreateAddSubscriptionModel(email, fullTimeTypeOfStudies);
            var res = service.AddSubscribtionAsync(model).Result;

            Assert.IsFalse(res.SubscribtionAdded);
            Assert.IsTrue(res.SubscribtionAlreadyExists);
            _fixture.SubscribersRepository.DidNotReceive().AddSubscribtionAsync(model.Email, model.TypeOfStudies.GetAbbrv());
        }
    }
}
