using System.Threading.Tasks;
using TimetableNotifier.Subscribers.Common;
using TimetableNotifier.Subscribers.Contracts;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Subscribers
{
    public class AddSubscribtionService
    {
        private readonly ISubscribersRepository _subscribersRepository;

        public AddSubscribtionService(ISubscribersRepository subscribersRepository)
        {
            _subscribersRepository = subscribersRepository;
        }

        public async Task<AddSubscriptionResult> AddSubscribtionAsync(AddSubscriptionModel model)
        {
            var result = new AddSubscriptionResult
            {
                Model = model
            };

            result.SubscribtionAlreadyExists = await _subscribersRepository.SubscribtionExistsAsync(model.Email, model.TypeOfStudies.GetAbbrv());

            if (!result.SubscribtionAlreadyExists)
                result.SubscribtionAdded = await _subscribersRepository.AddSubscribtionAsync(model.Email, model.TypeOfStudies.GetAbbrv());

            return result;
        }
    }
}