﻿using System.Threading.Tasks;
using TimetableNotifier.Subscribers.Common;
using TimetableNotifier.Subscribers.Contracts;

namespace TimetableNotifier.Subscribers
{
    public class RemoveSubscriptionsByEmailService
    {
        private readonly ISubscribersRepository _subscribersRepository;

        public RemoveSubscriptionsByEmailService(ISubscribersRepository subscribersRepository)
        {
            _subscribersRepository = subscribersRepository;
        }

        public async Task<RemoveSubscriptionsByEmailResult> RemoveSubscribtionsByEmailAsync(RemoveSubscriptionsByEmailModel model)
        {
            var result = new RemoveSubscriptionsByEmailResult { Model = model };
            result.Success = await _subscribersRepository.RemoveSubscribtionsByEmailAsync(model.Email);
            return result;
        }
    }
}