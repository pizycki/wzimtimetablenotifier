﻿using System.Threading.Tasks;
using TimetableNotifier.Subscribers.Common;
using TimetableNotifier.Subscribers.Contracts;

namespace TimetableNotifier.Subscribers
{
    public class SubscribersService : ISubscribersService
    {
        private readonly ISubscribersRepository _subscribersRepositoryAsync;

        public SubscribersService(ISubscribersRepository subscribersRepositoryAsync)
        {
            _subscribersRepositoryAsync = subscribersRepositoryAsync;
        }

        public AddSubscriptionResult AddSubscribtion(AddSubscriptionModel addSubscribtionModel)
        {
            return Task.Run(() => new AddSubscribtionService(_subscribersRepositoryAsync)
                                                            .AddSubscribtionAsync(addSubscribtionModel))
                                                            .Result;
        }

        public RemoveSubscriptionsByEmailResult RemoveSubscribtionsByEmail(RemoveSubscriptionsByEmailModel model)
        {
            return Task.Run(() => new RemoveSubscriptionsByEmailService(_subscribersRepositoryAsync)
                                                                        .RemoveSubscribtionsByEmailAsync(model))
                                                                        .Result;
        }
    }
}
