﻿using System;
using System.Configuration;

namespace TimetableNotifier.Common
{
    public static class AppConfiguration
    {
        public static string GetValueFor(string configKey)
        {
            return ConfigurationManager.AppSettings[configKey];
        }
        
        public static string WzimTimetableUrl => GetValueFor("WzimTimetableUrl");
        
        public static string SqlDbConnString => GetValueFor("SqlConnString");

        public static string SendMail => GetValueFor("SendEmail");

        public static string NewTimetableMailTemplate => GetValueFor("NewTimetable");

        public static bool IsWebAppDownForMaintanance => Convert.ToBoolean(GetValueFor("IsWebAppDownForMaintanance"));

        public static string CheckTimetableNotifierJobInterval => GetValueFor("CheckTimetableNotifierJobInterval");
    }
}