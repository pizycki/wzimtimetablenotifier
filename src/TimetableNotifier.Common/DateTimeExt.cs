﻿using System;

namespace TimetableNotifier.Common
{
    public static class DateTimeExt
    {
        /// <summary>
        /// Creates new instance of DateTime with kind.
        /// </summary>
        public static DateTime ChangeKind(this DateTime @this, DateTimeKind kind)
        {
            return new DateTime(@this.Year, @this.Month, @this.Day, @this.Hour, @this.Minute, @this.Second, kind);
        }
    }
}
