﻿namespace TimetableNotifier.Common
{
    public class EmailConstants
    {
        public const string EMAIL_REGEX = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
    }
}
