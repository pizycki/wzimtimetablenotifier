﻿using System.Text.RegularExpressions;
using NUnit.Framework;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Wzim.Tests
{
    [TestFixture]
    public class TimetabelLabelGenerator_Tests
    {
        private TimetabelLabelGenerator_Tests_Fixture _fixture;

        [SetUp]
        public void Setup()
        {
            _fixture = new TimetabelLabelGenerator_Tests_Fixture();
        }

        [TearDown]
        public void TearDown()
        {
            _fixture = null;
        }

        [Test]
        public void Label_contains_previous_and_actual_year_when_its_2nd_half_of_year()
        {
            _fixture.SetDateTimeTo_2015_September();
            var generator = _fixture.CreateTimetableLabelGenerator();
            var label = generator.GenerateLabel(TypeOfStudies.FullTime);
            Assert.IsTrue(label.Contains("2014_15"));
        }

        [Test]
        public void Label_contains_actual_and_next_year_when_its_1st_half_of_year()
        {
            _fixture.SetDateTimeTo_2016_January();
            var generator = _fixture.CreateTimetableLabelGenerator();
            var label = generator.GenerateLabel(TypeOfStudies.FullTime);
            Assert.IsTrue(label.Contains("2015_16"));
        }

        [Test]
        public void Label_contains_winter_when_its_2nd_half_of_year()
        {
            _fixture.SetDateTimeTo_2015_September();
            var generator = _fixture.CreateTimetableLabelGenerator();
            var label = generator.GenerateLabel(TypeOfStudies.FullTime);
            Assert.IsTrue(label.Contains("zima"));
        }

        [Test]
        public void Label_contains_summer_when_its_1st_half_of_year()
        {
            _fixture.SetDateTimeTo_2016_January();
            var generator = _fixture.CreateTimetableLabelGenerator();
            var label = generator.GenerateLabel(TypeOfStudies.FullTime);
            Assert.IsTrue(label.Contains("lato"));
        }

        [TestCase(TypeOfStudies.FullTime)]
        [TestCase(TypeOfStudies.PartTime)]
        public void Label_has_correct_TypeOfStudies(TypeOfStudies typeOfStudies)
        {
            _fixture.SetDateTimeTo_2016_January();
            var generator = _fixture.CreateTimetableLabelGenerator();
            var label = generator.GenerateLabel(typeOfStudies);
            Assert.IsTrue(label.Contains(typeOfStudies.GetAbbrv()));
        }

        [Test]
        public void Label_has_valid_format()
        {
            _fixture.SetDateTimeTo_2016_January();
            var generator = _fixture.CreateTimetableLabelGenerator();
            var label = generator.GenerateLabel(TypeOfStudies.FullTime);

            var match = Regex.Match(label, @"(Plan)\s(STAC|ZAO)\s(lato|zima)\s([0-9]{4}_[0-9]{2})");

            // Group[0]: Plan STAC lato 2015_16
            Assert.IsTrue(match.Success);
            Assert.AreEqual("Plan", match.Groups[1].Value);
            Assert.AreEqual("STAC", match.Groups[2].Value);
            Assert.AreEqual("lato", match.Groups[3].Value);
            Assert.AreEqual("2015_16", match.Groups[4].Value);
        }
    }
}
