﻿using System.IO;
using System.Threading.Tasks;
using TimetableNotifier.Html.Contracts;

namespace TimetableNotifier.Wzim.Tests
{
    internal class FileWebpageReader : IWebpageDownloader
    {
        public async Task<string> GetWebpageSourceCode(string url)
        {
            return await Task.FromResult(File.ReadAllText(url));
        }
    }
}