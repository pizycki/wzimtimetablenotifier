﻿using System;
using NSubstitute;

namespace TimetableNotifier.Wzim.Tests
{
    internal class TimetabelLabelGenerator_Tests_Fixture
    {
        private readonly IDateTimeProvider dateTimeProvider;

        internal TimetabelLabelGenerator_Tests_Fixture()
        {
            dateTimeProvider = Substitute.For<IDateTimeProvider>();
        }

        internal ITimetabelLabelGenerator CreateTimetableLabelGenerator()
        {
            return new TimetabelLabelGenerator(dateTimeProvider);
        }

        internal void SetDateTimeTo(DateTime dateTime)
        {
            dateTimeProvider.GetCurrentDateTime().Returns(dateTime);
        }

        internal void SetDateTimeTo_2015_September()
        {
            SetDateTimeTo(new DateTime(2014, 8, 25));
        }

        internal void SetDateTimeTo_2016_January()
        {
            SetDateTimeTo(new DateTime(2016, 1, 15));
        }
    }

}