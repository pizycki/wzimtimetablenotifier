﻿using System;
using System.Threading.Tasks;
using NSubstitute;
using Simple.Data;
using TimetableNotifier.Html;
using TimetableNotifier.Html.Contracts;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Wzim.Tests
{
    public class WzimTimetableChecker_Tests_Fixture
    {
        private readonly IHtmlService _htmlSrv;
        private readonly ITimetableRepository _timetableRepo;
        private readonly ITimetabelLabelGenerator _timetabelLabelGenerator;

        public WzimTimetableChecker_Tests_Fixture()
        {
            IWebpageDownloader webpageDownloader = new FileWebpageReader();
            _htmlSrv = new HtmlService(webpageDownloader);
            _timetabelLabelGenerator = Substitute.For<ITimetabelLabelGenerator>();
            _timetableRepo = Substitute.For<ITimetableRepository>();
        }

        internal WzimTimetableChecker CreateWzimTimetableChecker()
        {
            return new WzimTimetableChecker(_htmlSrv,
                                            _timetableRepo,
                                            _timetabelLabelGenerator);
        }

        internal void Label_generator_will_return(string label, TypeOfStudies typeOfStudies)
        {
            _timetabelLabelGenerator.GenerateLabel(typeOfStudies).Returns(label);
        }
        internal void Label_generator_will_return_Plan_TRYB_zima_2015_16()
        {
            Label_generator_will_return("Plan STAC zima 2015_16", TypeOfStudies.FullTime);
            Label_generator_will_return("Plan ZAO zima 2015_16", TypeOfStudies.PartTime);
        }

        public void Repository_has_the_latest_timetable_for(TypeOfStudies typeOfStudies, DateTime publishDateTime, string label = "label", string href = "href")
        {
            _timetableRepo.GetLatestTimetableFor(typeOfStudies).Returns(new Timetable(href, label, publishDateTime, typeOfStudies));
        }
    }
}
