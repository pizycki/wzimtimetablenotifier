﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Wzim.Tests
{
    [TestFixture]
    public class WzimTimetableChecker_Tests
    {
        const string url = @"WzimTimetableLists\Plan_STAC_ZAO_zima_2015_16.html";

        private WzimTimetableChecker_Tests_Fixture _fixture;

        [SetUp]
        public void Setup()
        {
            _fixture = new WzimTimetableChecker_Tests_Fixture();
        }

        [TearDown]
        public void TearDown()
        {
            _fixture = null;
        }

        [Test]
        public void Retrieves_new_timetables_when_repo_empty()
        {
            _fixture.Label_generator_will_return_Plan_TRYB_zima_2015_16();
            var checker = _fixture.CreateWzimTimetableChecker();
            var timetables = checker.GetNewTimetables(url).Result.ToList();
            Assert.AreEqual(timetables.Count, 2, "There should be 2 new timetables.");
        }

        [Test]
        public void Retrieves_new_one_timetable_when_the_other_one_is_present_in_repo()
        {
            _fixture.Label_generator_will_return_Plan_TRYB_zima_2015_16();
            _fixture.Repository_has_the_latest_timetable_for(TypeOfStudies.FullTime, new DateTime(2015, 2, 13, 0, 0, 0, DateTimeKind.Utc));
            var checker = _fixture.CreateWzimTimetableChecker();
            var timetables = checker.GetNewTimetables(url).Result.ToList();
            Assert.AreEqual(timetables.Count, 1, "There should be 1 new timetable.");
            Assert.AreEqual(TypeOfStudies.PartTime, timetables.Single().TypeOfStudies, $"The type of studies should be {TypeOfStudies.PartTime.GetAbbrv()}");
        }

        [Test]
        public void Retrieves_no_new_timetables_when_both_present_in_repo()
        {
            _fixture.Label_generator_will_return_Plan_TRYB_zima_2015_16();
           _fixture.Repository_has_the_latest_timetable_for(TypeOfStudies.FullTime, new DateTime(2015, 2, 13, 0, 0, 0, DateTimeKind.Utc));
            _fixture.Repository_has_the_latest_timetable_for(TypeOfStudies.PartTime, new DateTime(2015, 2, 13, 0, 0, 0, DateTimeKind.Utc));
            var checker = _fixture.CreateWzimTimetableChecker();
            var timetables = checker.GetNewTimetables(url).Result.ToList();
            Assert.AreEqual(timetables.Count, 0, "There should be no new timetable.");
        }

        private ITimetable GetTimetable_PartTime_zima_2015_16_when_FullTime_present()
        {
            _fixture.Label_generator_will_return_Plan_TRYB_zima_2015_16();
            _fixture.Repository_has_the_latest_timetable_for(TypeOfStudies.FullTime, new DateTime(2015, 2, 13, 0, 0, 0, DateTimeKind.Utc));
            var checker = _fixture.CreateWzimTimetableChecker();
            return checker.GetNewTimetables(url).Result.Single();
        }

        [Test]
        public void Retrieved_timetable_has_correct_typeOfStudies()
        {
            var timetable= GetTimetable_PartTime_zima_2015_16_when_FullTime_present();
            Assert.AreEqual(TypeOfStudies.PartTime, timetable.TypeOfStudies);
        }

        [Test]
        public void Retrieved_timetable_has_label()
        {
            var timetables= GetTimetable_PartTime_zima_2015_16_when_FullTime_present();
            Assert.IsFalse(string.IsNullOrWhiteSpace(timetables.Label));
        }       
        
        [Test]
        public void Retrieved_timetable_has_assigned_publishDate()
        {
            var timetable= GetTimetable_PartTime_zima_2015_16_when_FullTime_present();
            Assert.IsTrue(timetable.PublishDateUtc != default(DateTime));
        }

        [Test]
        public void Retrieved_timetable_publishDate_is_in_UTC()
        {
            var timetable= GetTimetable_PartTime_zima_2015_16_when_FullTime_present();
            Assert.IsTrue(timetable.PublishDateUtc.Kind == DateTimeKind.Utc);
        }
    }
}
