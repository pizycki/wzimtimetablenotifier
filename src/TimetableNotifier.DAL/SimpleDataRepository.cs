namespace TimetableNotifier.DAL
{
    public abstract class SimpleDataRepository
    {
        protected readonly dynamic _db;
        protected SimpleDataRepository(IDatabaseProvider dbProvider)
        {
            _db = dbProvider.GetDatabase();
        }
    }
}