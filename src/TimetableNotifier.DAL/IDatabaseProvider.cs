namespace TimetableNotifier.DAL
{
    public interface IDatabaseProvider
    {
        object GetDatabase();
    }
}