﻿using Simple.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimetableNotifier.Wzim;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.DAL
{
    public class TimetablesRepository : SimpleDataRepository, ITimetableRepository
    {
        public TimetablesRepository(IDatabaseProvider dbProvider) : base(dbProvider)
        {
        }

        public virtual async Task<ITimetable> GetLatestTimetableFor(TypeOfStudies typeOfStudies)
        {
            IEnumerable<SimpleRecord> rows = await Task.FromResult(_db.Timetables
                                                                .FindAllByTypeOfStudies(typeOfStudies.GetAbbrv())
                                                                .OrderByPublishDateUtcDescending());

            // In Simple.Data v 0.18, First and FirstOrDefault is not supported,
            // therefore we're forced to load all timetables from DB
            // and retrieve the latest one only.

            // TODO check with SQL adapters

            dynamic dbo = rows.Any()
                        ? rows.First()
                        : null;

            return dbo == null
                ? null
                : new Timetable(
                    dbo.Href,
                    dbo.Label,
                    dbo.PublishDateUtc,
                    TypeOfStudiesExt.ParseByAbbrv(dbo.TypeOfStudies));
        }

        public async Task<bool> AddTimetable(ITimetable timetable)
        {
            return await Task.FromResult(_db.Timetables.Insert(
                                            Label: timetable.Label,
                                            TypeOfStudies: timetable.TypeOfStudies.GetAbbrv(),
                                            PublishDateUtc: timetable.PublishDateUtc,
                                            Href: timetable.Href
                                            )) != null;
        }
    }
}
