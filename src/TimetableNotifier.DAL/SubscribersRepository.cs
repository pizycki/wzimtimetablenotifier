﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TimetableNotifier.Subscribers.Contracts;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.DAL
{
    public class SubscribersRepository : SimpleDataRepository, ISubscribersRepository
    {
        public SubscribersRepository(IDatabaseProvider dbProvider) : base(dbProvider)
        {
        }

        public async Task<IEnumerable<string>> GetSubscribersEmails(TypeOfStudies typeOfStudies)
        {
            var dbos = await Task.FromResult(_db.Subscribers.FindAll(_db.Subscribers.TypeOfStudies == typeOfStudies.GetAbbrv()));

            if (dbos == null) return null;

            var ls = new List<string>();
            foreach (var dbo in dbos)
            {
                ls.Add(dbo.Email);
            }
            return ls;
        }

        public async Task<bool> SubscribtionExistsAsync(string email, string typeOfStudies)
        {
            var entity = await Task.FromResult<dynamic>(_db.Subscribers.Find(
                                                                            _db.Subscribers.Email == email
                                                                            && _db.Subscribers.TypeOfStudies == typeOfStudies));

            return entity != null;
        }

        public async Task<bool> AddSubscribtionAsync(string email, string typeOfStudies)
        {
            return await Task.FromResult<bool>(_db.Subscribers.Insert(Email: email, TypeOfStudies: typeOfStudies) != null);
        }

        public async Task<bool> RemoveSubscribtionsByEmailAsync(string email)
        {
            return await Task.FromResult<bool>(_db.Subscribers.DeleteAll(_db.Subscribers.Email == email) != null);
        }
    }
}
