using System;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Wzim
{
    [ToString]
    public class Timetable : ITimetable
    {
        public Timetable(string href, string label, DateTime publishDateUtc, TypeOfStudies typeOfStudies)
        {
            TypeOfStudies = typeOfStudies;
            Href = href;
            Label = label;
            PublishDateUtc = publishDateUtc;
        }

        public TypeOfStudies TypeOfStudies { get; private set; }
        public string Href { get; private set; }
        public string Label { get; private set; }
        public DateTime PublishDateUtc { get; private set; }
    }
}