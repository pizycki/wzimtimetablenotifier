﻿using System.Collections.Generic;
using System.Linq;
using TimetableNotifier.Email.Contracts;
using TimetableNotifier.Subscribers.Contracts;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Wzim
{
    public class WzimTimetableNotifier : IWzimTimetableNotifier
    {
        private readonly INotificationSubscribersProvider _notificationSubscribersProvider;
        private readonly IEmailNotifier _emailNotifier;

        public WzimTimetableNotifier(INotificationSubscribersProvider notificationSubscribersProvider,
                                        IEmailNotifier emailNotifier)
        {
            _notificationSubscribersProvider = notificationSubscribersProvider;
            _emailNotifier = emailNotifier;
        }

        public async void NotifyAboutNewTimetable(ITimetable timetable)
        {
            // Get appropriate subscribers
            var subscribers = await _notificationSubscribersProvider.GetSubscribersEmails(timetable.TypeOfStudies);

            // Send them notifications
            SendNotifications(subscribers, timetable);
        }

        private void SendNotifications(IEnumerable<string> subscribers, ITimetable newTimetable)
        {
            subscribers.AsParallel().ForAll(address => _emailNotifier.SendNotificationEmailAsync(address, newTimetable));
        }
    }
}
