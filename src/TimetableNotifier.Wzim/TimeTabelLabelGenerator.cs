using System;
using System.Linq;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Wzim
{
    public class TimetabelLabelGenerator : ITimetabelLabelGenerator
    {
        private readonly IDateTimeProvider _dateTimeProvider;

        public TimetabelLabelGenerator(IDateTimeProvider dateTimeProvider)
        {
            _dateTimeProvider = dateTimeProvider;
        }

        private bool IsFirstHalfOfYearToday
        {
            get
            {
                var month = _dateTimeProvider.GetCurrentDateTime().Month;
                return month > 0 && month < 7;
            }
        }

        public string GenerateLabel(TypeOfStudies type)
        {
            // Pattern: Plan <type> <season> <year1>_<year2>
            // i.e.     Plan STAC lato 2014_15
            var season = GetSeason();
            var years = GetYears();

            var pattern = "Plan {0} {1} {2}_{3}";
            return string.Format(pattern, type.GetAbbrv(), season, years.Item1, TakeTwoLastDigits(years.Item2));
        }

        private static string TakeTwoLastDigits(int number)
        {
            return new string(number.ToString().Skip(2).ToArray());
        }

        private string GetSeason()
        {
            return IsFirstHalfOfYearToday ? "lato" : "zima";
        }

        /// <summary>
        /// Gets pair of years depending on what part of year (1st or 2nd) actualy is.
        /// F.e. { 2014, 2015 } if it's first year and 2015.
        /// </summary>
        private Tuple<int, int> GetYears()
        {
            if (IsFirstHalfOfYearToday)
            {
                // Take prev and present year
                var presentYear = _dateTimeProvider.GetCurrentDateTime().Year;
                var prevYear = presentYear - 1;
                return new Tuple<int, int>(prevYear, presentYear);
            }
            else
            {
                // Take present and following year
                var presentYear = _dateTimeProvider.GetCurrentDateTime().Year;
                var followingYear = presentYear + 1;
                return new Tuple<int, int>(presentYear, followingYear);
            }
        }
    }
}