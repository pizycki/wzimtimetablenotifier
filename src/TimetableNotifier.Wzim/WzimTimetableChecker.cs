﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using TimetableNotifier.Common;
using TimetableNotifier.Html.Contracts;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Wzim
{
    public class WzimTimetableChecker : IWzimTimetableChecker
    {
        private readonly IHtmlService _htmlSrv;
        private readonly ITimetableRepository _timetableRepo;
        private readonly ITimetabelLabelGenerator _tabelLabelGenerator;

        public WzimTimetableChecker(IHtmlService htmlService,
                                    ITimetableRepository timetableRepo,
                                    ITimetabelLabelGenerator tabelLabelGenerator)
        {
            _timetableRepo = timetableRepo;
            _tabelLabelGenerator = tabelLabelGenerator;
            _htmlSrv = htmlService;
        }

        public async Task<IEnumerable<ITimetable>> GetNewTimetables(string timetablesListUrl)
        {
            var html = await _htmlSrv.GetPageSource(timetablesListUrl);
            if (string.IsNullOrEmpty(html))
                throw new Exception("Parsed html is empty or null.");

            // Parse html code to HTML document
            var htmlDoc = _htmlSrv.ParseHtml(html);

            // Get timetable file sections
            var timetableHtmlNodes = GetTimetableHtmlNodes(htmlDoc);

            // Filter only actual season time tables
            var actualSeasonTimetablesInfos = GetActualSeasonTimetables(timetableHtmlNodes);

            var newTimetables = new List<ITimetable>();
            newTimetables.AddRange(from timetableInfo in actualSeasonTimetablesInfos
                                   let isNewer = CheckIsTimetableNewerAsync(timetableInfo)
                                   where isNewer.Result
                                   select timetableInfo);
            return newTimetables;
        }

        private async Task<bool> CheckIsTimetableNewerAsync(ITimetable timetable)
        {
            // Get the latest date of timetable and compare
            var latest = await _timetableRepo.GetLatestTimetableFor(timetable.TypeOfStudies);

            if (latest == null) // No previous timetable, nothing to compare, this is the newest.
                return true;

            return latest.PublishDateUtc < timetable.PublishDateUtc;
        }

        private IEnumerable<ITimetable> GetActualSeasonTimetables(IEnumerable<HtmlNode> htmlNodes)
        {
            return htmlNodes.Select(GetTimetableInfo)
                            .Where(CheckIsTimetableSeasonActual)
                            .ToList();
        }

        private bool CheckIsTimetableSeasonActual(ITimetable timetable)
        {
            var newTimetableLabel = _tabelLabelGenerator.GenerateLabel(timetable.TypeOfStudies);
            return timetable.Label == newTimetableLabel;
        }

        private static TypeOfStudies GetTypeOfStudiesByLabel(string label)
        {
            foreach (var type in from type in TypeOfStudiesExt.TypeOfStudiesDict.Keys
                                 let typeLabel = type.GetAbbrv()
                                 where label.Contains(typeLabel)
                                 select type) return type;

            throw new ArgumentException("Unknown label.");
        }

        private static ITimetable GetTimetableInfo(HtmlNode node)
        {
            var descendants = node.Descendants().ToArray();
            var publishDate = ParsePublishDate(descendants[45]);
            var label = ParseLabel(descendants[7]);
            var href = ParseHref(descendants[11]);

            var ttf = new Timetable(href, label, publishDate, GetTypeOfStudiesByLabel(label));

            Debug.WriteLine(ttf);

            return ttf;
        }

        private static string ParseHref(HtmlNode htmlNode)
        {
            return htmlNode.Attributes["href"].Value;
        }

        private static DateTime ParsePublishDate(HtmlNode htmlNode)
        {
            // \r\n\t\t13 Feb 2015\t\t
            var date = Regex.Match(htmlNode.InnerText, DATE_REGEX).Captures[0].Value;
            return DateTime.Parse(date)
                            .ChangeKind(DateTimeKind.Utc);

        }

        private static string ParseLabel(HtmlNode htmlNode)
        {
            // &nbsp;\r\n\t\t\t\t\t\t&nbsp;Plan STAC zima 2015_16&nbsp;\r\n\t\t\t\t\t
            var parsedText = Regex.Match(htmlNode.InnerText, TIMETABLE_REGEX).Captures[0].Value;
            // Plan STAC zima 2015_16
            return parsedText;
        }

        private static IEnumerable<HtmlNode> GetTimetableHtmlNodes(HtmlDocument htmlDocument)
        {
            var timetableNodes = new List<HtmlNode>();
            timetableNodes.AddRange(htmlDocument
                            .DocumentNode
                            .SelectNodes("//table[@class='sectiontableentry1']"));
            timetableNodes.AddRange(htmlDocument
                            .DocumentNode
                            .SelectNodes("//table[@class='sectiontableentry2']"));

            return from node in timetableNodes
                   let fileLabel = node.Descendants("#text").ToArray()[5]
                   where Regex.IsMatch(fileLabel.InnerText, TIMETABLE_REGEX)
                   select node;
        }

        const string DATE_REGEX = @"[0-9]{1,2} [A-Z][a-z]{2} \d{4}";
        const string TIMETABLE_REGEX = @"Plan (?:STAC|ZAO) (?:lato|zima) [0-9]{4}_[0-9]{2}";
    }
}