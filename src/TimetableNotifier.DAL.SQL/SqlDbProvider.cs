﻿using Simple.Data;

namespace TimetableNotifier.DAL.SQL
{
    public class SqlDbProvider : IDatabaseProvider
    {
        private readonly string _connString;

        public SqlDbProvider(string connString)
        {
            _connString = connString;
        }

        public object GetDatabase()
        {
            return Database.OpenConnection(_connString);
        }
    }
}
