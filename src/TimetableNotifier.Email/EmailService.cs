﻿using System;
using System.Net.Mail;
using SendGrid;
using TimetableNotifier.Email.Contracts;

namespace TimetableNotifier.Email
{
    public class EmailService : IEmailService
    {
        private readonly string _apiKey;

        public EmailService(string apiKey)
        {
            if (string.IsNullOrWhiteSpace(apiKey))
                throw new ArgumentException("apiKey");
            _apiKey = apiKey;
        }

        public async void SendEmail(IEmailMessage emailMessage)
        {
            var sgMsg = MapToSendGridMessage(emailMessage);
            var transportWeb = new Web(_apiKey);
            await transportWeb.DeliverAsync(sgMsg);
        }

        private static SendGridMessage MapToSendGridMessage(IEmailMessage emailMessage)
        {
            return new SendGridMessage
            {
                From = new MailAddress(emailMessage.From),
                Html = emailMessage.Html,
                Subject = emailMessage.Subject,
                To = new[] { new MailAddress(emailMessage.To), }
            };
        }
    }
}