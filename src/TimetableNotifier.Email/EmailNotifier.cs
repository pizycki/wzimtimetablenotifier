﻿using System.IO.Abstractions;
using System.Threading.Tasks;
using TimetableNotifier.Common;
using TimetableNotifier.Email.Common;
using TimetableNotifier.Email.Contracts;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Email
{
    public class EmailNotifier : IEmailNotifier
    {
        private readonly IEmailService _emailSrv;
        private readonly IMailTemplatesProvider _mailTemplatesProvider;

        public EmailNotifier(IEmailService emailSrv, IMailTemplatesProvider _mailTemplatesProvider)
        {
            _emailSrv = emailSrv;
            this._mailTemplatesProvider = _mailTemplatesProvider;
        }

        public async void SendNotificationEmailAsync(string address, ITimetable newTimetable)
        {
            var email = CreateEmail(address, newTimetable);
            await Task.Run(() => _emailSrv.SendEmail(email));
        }

        private IEmailMessage CreateEmail(string address, ITimetable newTimetable)
        {
            var msg = new EmailMessage();
            msg.To = address;
            msg.From = AppConfiguration.SendMail;
            msg.Subject = "Dostępny nowy plan WZIM - (" + newTimetable.Label + ")";
            msg.Html = GetMailBody(newTimetable);

            return msg;
        }

        // TODO Add Fody method cache https://github.com/Dresel/MethodCache
        private string GetMailBody(ITimetable newTimetable)
        {
            var template = _mailTemplatesProvider.GetTemplateFor("NewTimetable");
            return string.Format(template, newTimetable.Label, newTimetable.Href);
        }
    }

    public class MailTemplatesProvider : IMailTemplatesProvider
    {
        private readonly IFileSystem _fileSystem;
        private readonly string _root;

        public MailTemplatesProvider(string root, IFileSystem fileSystem)
        {
            _root = root;
            _fileSystem = fileSystem;
        }

        public string GetTemplateFor(string name)
        {
            var path = GetFilePath(name);
            var template = GetFileContent(path);
            return template;
        }

        private string GetFilePath(string name)
        {
            return $"{_root}/MailTemplates/{name}.html";
        }

        private string GetFileContent(string path)
        {
            using (var stream = _fileSystem.File.OpenText(path))
            {
                return stream.ReadToEnd();
            }
        }
    }
}
