using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using TimetableNotifier.Html.Contracts;

namespace TimetableNotifier.Html
{
    public class HtmlService : IHtmlService
    {
        private readonly IWebpageDownloader _webpageDownloader;
        public HtmlService(IWebpageDownloader webpageDownloader)
        {
            _webpageDownloader = webpageDownloader;
        }

        public async Task<string> GetPageSource(string url)
        {
            return await _webpageDownloader.GetWebpageSourceCode(url);
        }

        public string RemoveWhiteHtmlMarks(string inputHtml)
        {
            var noHTML = Regex.Replace(inputHtml, @"<[^>]+>|&nbsp;", "");
            var noHTMLNormalised = Regex.Replace(noHTML, @"\s{2,}", " ");
            return noHTMLNormalised;
        }

        public HtmlDocument ParseHtml(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            return doc;
        }

    }

    public class WebpageDownloader : IWebpageDownloader
    {
        public async Task<string> GetWebpageSourceCode(string url)
        {
            using (var client = new WebClient())
            {
                return await client.DownloadStringTaskAsync(new Uri(url));
            }
        }
    }
}