﻿using AutoMapper;
using TimetableNotifier.Subscribers.Common;
using TimetableNotifier.WebApp.Models.Subscribers;

namespace TimetableNotifier.WebApp
{
    public class Mappings
    {
        public static void Create()
        {
            Mapper.CreateMap<AddSubscribtionVM, AddSubscriptionModel>();
            Mapper.CreateMap<RemoveSubscribtionVM, RemoveSubscriptionsByEmailModel>();
        }
    }
}