﻿using System.Web.Mvc;
using System.Web.Routing;
using TimetableNotifier.Common;

namespace TimetableNotifier.WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            if (AppConfiguration.IsWebAppDownForMaintanance)
            {
                routes.MapRoute("Offline", "{controller}/{action}/{id}",
                new
                {
                    action = MVC.Home.ActionNames.Offline,
                    controller = MVC.Home.Name,
                    id = UrlParameter.Optional
                });
            }


            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new
                {
                    controller = MVC.Subscribers.Name,
                    action = MVC.Subscribers.Actions.ActionNames.Index,
                    id = UrlParameter.Optional
                }
            );
        }
    }
}
