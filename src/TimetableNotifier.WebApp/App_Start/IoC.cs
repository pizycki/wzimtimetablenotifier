﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using NLog;
using TimetableNotifier.Common;
using TimetableNotifier.DAL;
using TimetableNotifier.DAL.SQL;
using TimetableNotifier.Email;
using TimetableNotifier.Email.Contracts;
using TimetableNotifier.Html;
using TimetableNotifier.Html.Contracts;
using TimetableNotifier.Subscribers;
using TimetableNotifier.Subscribers.Contracts;
using TimetableNotifier.Wzim;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.WebApp
{
    public class IoC
    {
        static IContainer _container;
        public static IContainer Container
        {
            get
            {
                if (_container == null)
                {
                    BuildContainer(); // As side effect
                }
                return _container;
            }
        }

        public static void BuildContainer()
        {
            var builder = GetBuilderWithServices();

            // Register your MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            _container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(_container));
        }

        public static ContainerBuilder GetBuilderWithServices()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<SqlDbProvider>()
                    .AsImplementedInterfaces()
                    .WithParameter("connString", AppConfiguration.SqlDbConnString)
                    .InstancePerRequest();

            builder.RegisterType<SubscribersService>()
                    .As<ISubscribersService>()
                    .InstancePerRequest();

            builder.RegisterType<SubscribersRepository>()
                    .As<ISubscribersRepository>()
                    .As<INotificationSubscribersProvider>()
                    .InstancePerRequest();

            builder.RegisterType<EmailNotifier>()
                    .As<IEmailNotifier>()
                    .InstancePerRequest();

            builder.RegisterType<WzimTimetableNotifier>()
                    .As<IWzimTimetableNotifier>()
                    .InstancePerRequest();

            builder.RegisterType<HtmlService>()
                    .As<IHtmlService>()
                    .InstancePerRequest();

            builder.RegisterType<TimetablesRepository>()
                    .As<ITimetableRepository>()
                    .InstancePerRequest();

            builder.RegisterType<WzimTimetableChecker>()
                    .As<IWzimTimetableChecker>()
                    .InstancePerRequest();

            builder.RegisterType<Logger>()
                    .As<ILogger>()
                    .InstancePerRequest();

            return builder;
        }
    }
}