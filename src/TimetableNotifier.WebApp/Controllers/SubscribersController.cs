﻿using AutoMapper;
using CaptchaMvc.HtmlHelpers;
using System.Collections.Generic;
using System.Web.Mvc;
using TimetableNotifier.Common;
using TimetableNotifier.Subscribers.Common;
using TimetableNotifier.Subscribers.Contracts;
using TimetableNotifier.WebApp.Models.Subscribers;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.WebApp.Controllers
{
    public partial class SubscribersController : Controller
    {
        private readonly ISubscribersService _subscribersSrv;

        public SubscribersController(ISubscribersService subscribersSrv)
        {
            _subscribersSrv = subscribersSrv;
        }

        [HttpGet]
        public virtual ActionResult Index()
        {
            return View();
        }

        #region ===== AddSubscribtion =====
        [HttpGet]
        public virtual ActionResult AddSubscribtion()
        {
            return PartialView(MVC.Subscribers.Views.ViewNames._AddSubscribtion, new AddSubscribtionVM());
        }

        [HttpPost]
        public virtual ActionResult AddSubscribtion(AddSubscribtionVM vm)
        {
            if (!this.IsCaptchaValid(Resources.InvalidCaptcha))
            {
                ViewBag.InvalidCaptchaMessage = Resources.InvalidCaptcha;
                return PartialView(MVC.Subscribers.Views.ViewNames._AddSubscribtion, vm);
            }

            if (!ModelState.IsValid)
                return PartialView(MVC.Subscribers.Views.ViewNames._AddSubscribtion, vm);

            var resultVM = AddMultipleSubscribtions(vm);

            return PartialView(MVC.Subscribers.Views.ViewNames._AddSubscribtion, resultVM);
        }

        [NonAction]
        private AddSubscribtionVM AddMultipleSubscribtions(AddSubscribtionVM vm)
        {
            var addSubscribtionResultVMs = new List<AddSubscribtionResultVM>();
            var model = Mapper.Map<AddSubscriptionModel>(vm);

            foreach (var type in model.TypeOfStudies.GetTypeOfStudiesCollection())
            {
                model.TypeOfStudies = type;
                var result = _subscribersSrv.AddSubscribtion(model);
                var resultVM = CreateResultVM(result);
                addSubscribtionResultVMs.Add(resultVM);
            }

            ResetAddSubscribtionVM(vm);
            vm.AddSubscribtionResultVMs = addSubscribtionResultVMs;
            return vm;
        }

        [NonAction]
        private void ResetAddSubscribtionVM(AddSubscribtionVM vm)
        {
            vm.TypeOfStudies = TypeOfStudies.FullTime;
            vm.Email = string.Empty;
        }

        [NonAction]
        private AddSubscribtionResultVM CreateResultVM(AddSubscriptionResult result)
        {
            var vm = new AddSubscribtionResultVM();
            if (result.SubscribtionAlreadyExists == false
                && result.SubscribtionAdded == false)
            {
                vm.CssClass = "alert-danger";
                vm.Message = Resources.AddSubscribtionResultVM_danger_Message;
                vm.Icon = "remove";
            }
            else if (result.SubscribtionAlreadyExists)
            {
                vm.CssClass = "alert-warning";
                vm.Message = string.Format(Resources.AddSubscribtionResult_warning_Message,
                    result.Model.Email);
                vm.Icon = "info-sign";
            }
            else
            {
                vm.CssClass = "alert-success";
                vm.Message = string.Format(Resources.AddSubscribtionResult_ok_Message,
                    result.Model.Email);
                vm.Icon = "ok";
            }
            return vm;
        }

        #endregion

        #region ===== RemoveSubscribtion =====
        [HttpGet]
        public virtual ActionResult RemoveSubscribtion()
        {
            return PartialView(MVC.Subscribers.Views.ViewNames._RemoveSubscribtion, new RemoveSubscribtionVM());
        }

        [HttpPost]
        public virtual ActionResult RemoveSubscribtion(RemoveSubscribtionVM vm)
        {
            if (!ModelState.IsValid)
                return PartialView(MVC.Subscribers.Views.ViewNames._RemoveSubscribtion, vm);

            var srvModel = Mapper.Map<RemoveSubscriptionsByEmailModel>(vm);
            var result = _subscribersSrv.RemoveSubscribtionsByEmail(srvModel);

            vm.Result = CreateResultVM(result);
            ResetRemoveSubscribtionVM(vm);

            return PartialView(MVC.Subscribers.Views.ViewNames._RemoveSubscribtion, vm);
        }

        private void ResetRemoveSubscribtionVM(RemoveSubscribtionVM vm)
        {
            vm.Email = string.Empty;
        }

        [NonAction]
        public RemoveSubscribtionResultVM CreateResultVM(RemoveSubscriptionsByEmailResult result)
        {
            return result.Success
                ? new RemoveSubscribtionResultVM
                {
                    CssClass = "alert-success",
                    Message = string.Format(Resources.RemoveSubscribtion_SubscribtionRemoved_Message, result.Model.Email),
                    Icon = "ok"
                }
                : new RemoveSubscribtionResultVM
                {
                    CssClass = "alert-warning",
                    Message = string.Format(Resources.RemoveSubscribtion_EmailNotFound_Message),
                    Icon = "info-sign"
                };
        }

        #endregion

    }
}