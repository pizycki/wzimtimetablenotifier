﻿using System.Web.Mvc;

namespace TimetableNotifier.WebApp.Controllers
{
    public partial class HomeController : Controller
    {
        public virtual ActionResult Offline()
        {
            return View();
        }
    }
}