﻿using System.ComponentModel.DataAnnotations;
using TimetableNotifier.Common;

namespace TimetableNotifier.WebApp.Models.Subscribers
{
    public class RemoveSubscribtionVM
    {
        [Required(ErrorMessageResourceName = "AddSubscribtionVM_Email_Required", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(EmailConstants.EMAIL_REGEX, ErrorMessageResourceName = "AddSubscribtionVM_Email_Regex", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "AddSubscribtionVM_Email", ResourceType = typeof(Resources))]
        public string Email { get; set; }

        public RemoveSubscribtionResultVM Result { get; set; }
    }

    public class RemoveSubscribtionResultVM
    {
        public string Message { get; set; }
        public string CssClass { get; set; }
        public string Icon { get; set; }

    }
}