﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TimetableNotifier.Common;
using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.WebApp.Models.Subscribers
{
    public class AddSubscribtionVM
    {
        [Required(ErrorMessageResourceName = "AddSubscribtionVM_Email_Required", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(EmailConstants.EMAIL_REGEX, ErrorMessageResourceName = "AddSubscribtionVM_Email_Regex", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "AddSubscribtionVM_Email", ResourceType = typeof(Resources))]
        public string Email { get; set; }

        [Display(Name = "AddSubscribtionVM_TypeOfStudies", ResourceType = typeof(Resources))]
        public TypeOfStudies TypeOfStudies { get; set; }

        public IEnumerable<AddSubscribtionResultVM> AddSubscribtionResultVMs { get; set; }
    }

    public class AddSubscribtionResultVM
    {
        public string Message { get; set; }
        public string CssClass { get; set; }
        public string Icon { get; set; }
    }
}