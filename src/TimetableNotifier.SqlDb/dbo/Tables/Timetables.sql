﻿CREATE TABLE [dbo].[Timetables] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [Label]          VARCHAR (MAX) NOT NULL,
    [TypeOfStudies]  VARCHAR (20)  NOT NULL,
    [PublishDateUtc] DATETIME2 (7) NOT NULL,
    [Href]           VARCHAR (MAX) NULL,
    CONSTRAINT [PK_Timetables] PRIMARY KEY CLUSTERED ([Id] ASC)
);

