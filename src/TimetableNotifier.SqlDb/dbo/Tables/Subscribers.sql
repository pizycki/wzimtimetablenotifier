﻿CREATE TABLE [dbo].[Subscribers] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [Email]           VARCHAR (MAX) NOT NULL,
    [TypeOfStudies]   VARCHAR (20)  NOT NULL,
    [CreationTimeUtc] DATETIME2 (7) NOT NULL,
    CONSTRAINT [PK_Subscribers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

