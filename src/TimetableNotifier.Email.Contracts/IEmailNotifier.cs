﻿using TimetableNotifier.Wzim.Contracts;

namespace TimetableNotifier.Email.Contracts
{
    public interface IEmailNotifier
    {
        void SendNotificationEmailAsync(string address, ITimetable newTimetable);
    }
}