namespace TimetableNotifier.Email.Contracts
{
    public interface IEmailMessage
    {
        string To { get; set; }
        string From { get; set; }
        string Subject { get; set; }
        string Html { get; set; }
    }
}