namespace TimetableNotifier.Email.Contracts
{
    public interface IMailTemplatesProvider
    {
        string GetTemplateFor(string name);

    }
}