﻿
namespace TimetableNotifier.Email.Contracts
{
    public interface IEmailService
    {
        void SendEmail(IEmailMessage emailMessage);
    }
}