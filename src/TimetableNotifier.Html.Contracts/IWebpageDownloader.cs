using System.Threading.Tasks;

namespace TimetableNotifier.Html.Contracts
{
    public interface IWebpageDownloader
    {
        Task<string> GetWebpageSourceCode(string url);
    }
}