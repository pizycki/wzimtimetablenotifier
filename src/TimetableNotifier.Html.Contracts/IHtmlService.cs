﻿using System.Threading.Tasks;
using HtmlAgilityPack;

namespace TimetableNotifier.Html.Contracts
{
    public interface IHtmlService
    {
        Task<string> GetPageSource(string url);
        HtmlDocument ParseHtml(string html);
        string RemoveWhiteHtmlMarks(string inputHtml);
    }
}