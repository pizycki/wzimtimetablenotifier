﻿using TimetableNotifier.Email.Contracts;

namespace TimetableNotifier.Email.Common
{
    [ToString]
    public class EmailMessage : IEmailMessage
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Html { get; set; }
    }
}