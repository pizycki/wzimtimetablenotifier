# WZIM Timetable Notifier
_Application for notifing about new timetable avaible on [Faculty of Applied Informatics and Mathematics website](http://www.wzim.sggw.pl/en/) at [Warsaw University of Life Sciences](http://www.sggw.pl/en/)_

**Application**
[http://wzimtimetablenotifier.azurewebsites.net/](http://wzimtimetablenotifier.azurewebsites.net/)

### Change log
#### 1.4
* *New:* [Hangfire](https://github.com/HangfireIO) scheduling system introduced
* *New:* Maintanance page for WebApp

#### 1.3
* *Major change:* Migration from NoSQL to SQL
* *Major change:* Emails queueing introduced
* *Fix:* Correct timetable date while parsing webpage, now it is UTC
* *Change:* Easier configuration

#### 1.2
* *Fix:* Checker was not retrieving the latest timetable.
* *Change*: Link to author website
* *Change*: Minimum window width required to change to horizontal layout is now greater.

#### 1.1
* *New:* Add subscribtions removal

### Requirments
* C# 6.0 compiler
* .NET 4.5.2
* Visual Studio (originaly developed on VS 2013 Enterprise)
* Azure SDK
* MS SQL

### TODO list
* Dodanie linku do strony dziekanatu
* Automatyczne zamykanie powiadomień po upływie 10 sek
* Dołączenie ikony dla urządzeń apple
* Załączenie w mailu linku do bezpośredniego pobrania pliku
* Zachowywanie nowych planów na dysku